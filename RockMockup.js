$(document).ready(function() {
  // handle mouseovers and outs
	$(document).on('mouseover', function(e) {
		var $el = $(e.target);

		// remove all old borders
		$('.mockupHover').removeClass('mockupHover');

		// return if we are outside main div
		if(!$el.closest('#main').length) return;

		// return if we are in an edit session
		if(editoractive) return;

		// add hoverclass to this element
		$el.addClass('mockupHover');
		updateSelector();
	}).on('mouseout', function(e) {
		var $el = $(e.target).closest('.mockupHover');
		$el.removeClass('mockupHover');
	});

  // add selectordiv element to body and handle updates
  $('body').append('<div id="selectorDiv"></div>');
	var updateSelector = function() {
		var $el = $('#main').find('.mockupHover');
		if(!$el.length) return;
		var str = '';
		str += $el.prop('nodeName');

		var id = $el.attr('id');
		if(id) str += '#' + id;

		var cls = ($el.attr('class')).split(' ').join('.');
		if(cls) str += '.' + cls;

		$('#selectorDiv').text(str);
	}

  // handlepresses
	var editoractive = false;
	$(this).keydown(function(e) {
		// if editor is active we dont intercept keydowns
		if(editoractive) return;

		var $el = $('#main').find('.mockupHover');
    if(!$el.length) return;
    
    // prevent default keypress behaviour
		e.preventDefault();

		var $newel;

		// c --> clone
		if(e.keyCode == 67) {
			$newel = $el.clone();
			$newel.insertAfter($el);
			$el.removeClass('mockupHover');
		}

		// arrow left --> select previous sibling
		if(e.keyCode == 37) {
			$newel = $el.prev();
			if(!$newel.length) return;
			$el.removeClass('mockupHover');
			$newel.addClass('mockupHover');
			e.stopPropagation();
		}

		// arrow up --> select parent
		if(e.keyCode == 38) {
			$newel = $el.parent();
			if(!$newel.closest('#main').length) return;
			$el.removeClass('mockupHover');
			$newel.addClass('mockupHover');
		}

		// arrow right --> select next sibling
		if(e.keyCode == 39) {
			$newel = $el.next();
			if(!$newel.length) return;
			$el.removeClass('mockupHover');
			$newel.addClass('mockupHover');
		}

		// arrow down --> select first child
		if(e.keyCode == 40) {
			$newel = $el.children().first();
			if(!$newel.length) return;
			$el.removeClass('mockupHover');
			$newel.addClass('mockupHover');
		}

		// r --> remove
		if(e.keyCode == 82) {
			$el.remove();
    }
    
    // e --> edit element
		if(e.keyCode == 69) {
      editoractive = true;
      $el.html('<textarea id="edittextarea" name="edittextarea">' + $el.html() + '</textarea>');
      CKEDITOR.config.autoParagraph = false;
      CKEDITOR.replace( 'edittextarea' );

      var editor = CKEDITOR.instances.edittextarea;
      editor.addCommand('saveanddestroy', {
        exec : function(editor, data) {
          var html = editor.getData();
          $el.html(html);
          editoractive = false;
        }
      });
      editor.setKeystroke( CKEDITOR.CTRL + 13, 'saveanddestroy');
    }

		updateSelector();
	});
});